const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const path = require('path');
const session = require('express-session');
const User = require("./models/user")
const userRouter = require('./routes/users');
const photoRouter = require('./routes/photos');
const indexRouter = require('./routes/index');

const LocalStrategy = require('passport-local').Strategy;


const app = express();
const port = 80;

const uri = "mongodb+srv://admin:8o1zZ9wQqwLfy52g@randomcluster.fuq1jes.mongodb.net/?retryWrites=true&w=majority";
const clientOptions = {}
mongoose.connect(uri, clientOptions);


app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'N325x!dsFsadHD62@8B445#JWTsghj764', // Change this to a secure random key
    resave: false,
    saveUninitialized: false,
}));

// Configure the local strategy
passport.use(new LocalStrategy(async (username, password, done) => {
    try {
      const user = await User.findOne({ username });
  
      if (!user || !user.comparePassword(password)) {
        return done(null, false, { message: 'Invalid credentials' });
      }
  
      return done(null, user);
    } catch (error) {
      return done(error);
    }
}));

passport.serializeUser((user, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
    User.findById(id)
    .then(user => {
      if (user) {
        return done(null, user);
      }
      return done(null, false);
    })
    .catch(err => console.log(err));
});

// Middleware to initialize Passport
app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs'); // Set EJS as the view engine
app.set('views', __dirname + '/views'); // Set the views directory

// Register routers
app.use('/users', userRouter);
app.use('/photos', photoRouter);
app.get('/', indexRouter)


app.listen(port, () => {
    console.log('Server is running at http://localhost:${port}');
});