const express = require('express');
const router = express.Router();
const homeController = require('../controllers/homeController');

// Index page route
router.get('/', homeController.index);

module.exports = router;