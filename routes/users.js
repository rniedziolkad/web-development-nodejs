const express = require('express');
const passport = require('passport');
const router = express.Router();
const userController = require('../controllers/userController');
const { ensureAuthenticated } = require('../controllers/auth');

// Register a new user
router.post('/register', userController.registerUser);

// Login user
router.post('/login', userController.loginUser);
router.get('/logout', userController.logoutUser);

// GET Forms
router.get('/register', userController.registerForm);
router.get('/login', userController.loginForm);
router.get('/profile/:username', userController.userProfile);


router.use(ensureAuthenticated)
// Change user password
router.post('/change-password', userController.changePassword);

// Change user description
router.post('/change-description', userController.changeDescription);

//GET
router.get('/change-password', userController.changePasswordForm);


module.exports = router