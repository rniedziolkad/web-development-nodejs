const express = require('express');
const photoController = require('../controllers/photoController');
const { ensureAuthenticated } = require('../controllers/auth');
const { upload } = require('../config/multer-config');

const router = express.Router();

// Route to render the form for adding a new photo
router.get('/add', ensureAuthenticated, photoController.addPhotoForm);

// Route to handle the submission of the new photo form
router.post('/add', upload.single('photoFile'), ensureAuthenticated, photoController.addPhoto);

// Route to view a specific photo
router.get('/list/:id', ensureAuthenticated, photoController.viewPhoto);

// Route to handle adding a new comment
router.post('/list/:id/add-comment', ensureAuthenticated, photoController.addComment);

// Route to handle petting dog
router.post('/list/:id/pet', ensureAuthenticated, photoController.pet);
router.post('/list/:id/delete', ensureAuthenticated, photoController.deletePhoto);
router.post('/list/:id/edit-caption', ensureAuthenticated, photoController.editCaption);

module.exports = router;