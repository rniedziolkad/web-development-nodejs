document.addEventListener('DOMContentLoaded', function() {
    // Add an event listener to the pet button
    document.querySelectorAll('.pet-button').forEach(function(button) {
        button.addEventListener('click', function(event) {
            event.preventDefault(); // Prevent the default form submission
            const petSection = event.currentTarget.closest('.pet-section');

            // Get the photo ID from the data attribute
            const photoId = this.closest('.pet-section').dataset.photoId;

            // Perform an asynchronous request (AJAX) to the pet endpoint
            fetch(`/photos/list/${photoId}/pet`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify({}), 
            })
            .then(response => {
              if (response.redirected) {
                  // If the response is a redirect, manually redirect the browser
                  window.location.href = response.url;
              } else {
                  // If not a redirect, proceed with handling the JSON response
                  return response.json();
              }
            })
            .then(data => {
                // Update the pet count in the UI
                const petCountElement = this.querySelector('.pet-count');
                petCountElement.textContent = data.petCount;

                petSection.classList.toggle('petted', data.hasPetted);

                // Remove the hover effect
                petSection.classList.remove('hovered');
            })
            .catch(error => console.error('Error:', error));
        });
    });
    document.querySelectorAll('.pet-section').forEach(function(petSection) {
      petSection.addEventListener('mouseleave', function() {
          // Add the 'hovered' class on mouse leave
          this.classList.remove('hovered');
      });

      petSection.addEventListener('mouseenter', function() {
          // Remove the 'hovered' class on mouse enter
          this.classList.add('hovered');
      });
  });
  });