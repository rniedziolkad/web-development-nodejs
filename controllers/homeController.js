const Photo = require('../models/photo');

const index = async (req, res) => {
  try {
    // Query all photos and populate the 'user' field
    const photos = await Photo.find().populate('user', 'username');

    // Sort photos based on the number of pets (likes)
    const sortedPhotos = photos.sort((a, b) => b.pets.length - a.pets.length);

    // Render the index page with sorted photos
    res.render('index', { photos: sortedPhotos , loggedInUser: req.user});
  } catch (error) {
    console.error(error);
    res.status(500).send('Internal Server Error');
  }
};

module.exports = {
    index,
};