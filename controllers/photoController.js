const Photo = require('../models/photo');
const Comment = require('../models/comment');

const addPhotoForm = (req, res) => {
  // Render the form to add a new photo
  res.render('photos/add-photo', {loggedInUser: req.user});
};

const addPhoto = async (req, res) => {
    try {
        // Check if the user is authenticated
        if (!req.isAuthenticated()) {
          return res.status(401).send('Unauthorized');
        }
    
        const user = req.user;
        const caption = req.body.caption;
        // Check if a file was uploaded
        if (!req.file) {
          return res.status(400).send('No file uploaded');
        }
        const imageUrl = `/images/${req.file.filename}`;
    
        // Create a new photo object
        const newPhoto = new Photo({
          user: user._id,
          caption: caption,
          imageUrl: imageUrl,
        });
    
        // Save the new photo to the database
        await newPhoto.save();
    
        // Redirect to the user's profile
        res.redirect(`/users/profile/${user.username}`);
      } catch (error) {
        console.error(error);
        res.status(500).send('Internal Server Error');
      }
};

const viewPhoto = async (req, res) => {
    try {
      const photoId = req.params.id;
  
      // Fetch the photo details along with user, comments, and likes
      const photo = await Photo.findById(photoId)
        .populate('user', 'username') // Populate the 'user' field with 'username'
        .populate({
          path: 'comments',
          populate: { path: 'user', select: 'username' }, // Populate 'comments.user' with 'username'
        })
        .populate('pets', 'username'); // Populate 'pets' array with 'username' of users who petted the dog
  
      if (!photo) {
        return res.status(404).send('Photo not found');
      }
  
      // Render the photo details page
      res.render('photos/view-photo', { photo , loggedInUser: req.user});
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
};

const addComment = async (req, res) => {
    try {
      const photoId = req.params.id;
      // Find the photo
      const photo = await Photo.findById(photoId);
      if (!photo) {
        return res.status(404).send('Photo not found');
      }

      const { commentText } = req.body;
  
      // Create a new comment
      const newComment = new Comment({
        user: req.user,
        photo: photo,
        text: commentText,
      });
  
      // Save the new comment
      await newComment.save();
      // Push the new comment
      photo.comments.push(newComment);
      // Save the updated photo with the new comment reference
      await photo.save();
  
      // Redirect back to view the photo after adding the comment
      res.redirect('/photos/list/'+photoId);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
};

const pet = async (req, res) => {
    try {
      const photoId = req.params.id;
      userId = req.user._id
  
      // Check if the user has already liked the photo
      const photo = await Photo.findById(photoId);
      if (!photo) {
        return res.status(404).send('Photo not found');
      }
  
      const hasLiked = photo.pets.includes(userId);
      if (hasLiked) {
        // User has already petted, remove
        photo.pets.pull(userId);
      } else {
        // User hasn't petted, pet the dog
        photo.pets.push(userId);
      }
      // Save the updated photo
      await photo.save();

      // Send a JSON response with the updated photo data
      res.json({ petCount: photo.pets.length, hasPetted: !hasLiked});
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
};

const deletePhoto = async (req, res) => {
    try {
      const photoId = req.params.id;
  
      // Find the photo and check if it exists
      const photo = await Photo.findById(photoId);
      if (!photo) {
        return res.status(404).send('Photo not found');
      }
  
      // Check if the logged-in user is the owner of the photo
      if (!req.user._id.equals(photo.user)) {
        return res.status(403).send('Permission denied');
      }
  
      // Remove all comments associated with the photo
      await Comment.deleteMany({ _id: { $in: photo.comments } });
  
      // Remove the photo itself
      await Photo.deleteOne(photo)
  
      // Redirect to a page or route after successful deletion
      res.redirect('/users/profile/'+req.user.username); // Redirect to the user profile
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
};

const editCaption = async (req, res) => {
    try {
      const photoId = req.params.id;
      const newCaption = req.body.newCaption;
  
      // Find the photo and check if it exists
      const photo = await Photo.findById(photoId);
      if (!photo) {
        return res.status(404).send('Photo not found');
      }
  
      // Check if the logged-in user is the owner of the photo
      if (!req.user._id.equals(photo.user)) {
        return res.status(403).send('Permission denied');
      }
  
      // Update the caption
      photo.caption = newCaption;
      await photo.save();
  
      // Redirect to the photo details page after successful edit
      res.redirect('/photos/list/'+photoId);
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal Server Error');
    }
};

module.exports = { 
    addPhotoForm, 
    addPhoto, 
    viewPhoto,
    addComment,
    pet,
    deletePhoto,
    editCaption,
};