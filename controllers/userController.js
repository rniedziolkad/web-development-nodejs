const passport = require('passport');
const User = require('../models/user');
const Photo = require('../models/photo');

// Register a new user
const registerUser = async (req, res) => {
    try {
      const { username, password, email, description } = req.body;
      console.log(username, password, email, description)
      // Check if the username or email is already taken
      const existingUser = await User.findOne({ $or: [{ username }, { email }] });
      console.log(existingUser)
      if (existingUser) {
        return res.status(400).json({ error: 'Username or email is already taken' });
      }
  
      // Create a new user
      const newUser = new User({ username, password, email, description });
      await newUser.save();
  
      res.redirect('/users/login');
    } catch (err) {
      res.status(500).json({ error: 'Internal Server Error' });
      console.log("ERROR", err)
    }
};

const loginUser = (req, res, next) => {
    passport.authenticate('local', { session: false }, (err, user, info) => {
      if (err) {
        return res.status(500).json({ error: err.message });
      }
  
      if (!user) {
        return res.status(401).json({ error: 'Invalid credentials' });
      }

      req.login(user, (err) => {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: 'Error creating session' });
        }

        return res.redirect('/');
      });
  
    })(req, res, next);
};

// Change user password
const changePassword = async (req, res) => {
  try {
    const { currentPassword, newPassword } = req.body;
    const user = req.user; // Assuming the user is authenticated using Passport and the JWT strategy

    // Verify the current password
    if (!await user.comparePassword(currentPassword)) {
      return res.redirect('/users/change-password?error=Incorrect current password');
    }

    // Update the password
    user.password = newPassword;
    await user.save();

    res.redirect('/users/change-password?message=Password changed successfully');
  } catch (error) {
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// Change user description
const changeDescription = async (req, res) => {
    try {
      const { newDescription } = req.body;
      const user = req.user; // Assuming the user is authenticated using Passport and the JWT strategy
  
      // Update the description
      user.description = newDescription;
      await user.save();
  
      res.redirect('/users/profile/'+user.username)
    } catch (error) {
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

//GET
const registerForm = (req, res) => {
    if (req.isAuthenticated()) {
    // Redirect to the user's profile page
      return res.redirect(`/users/profile/${req.user.username}`);
    }
    // Render the registration form
    res.render('users/register', {loggedInUser: req.user}); 
};
  
const loginForm = (req, res) => {
    if (req.isAuthenticated()) {
    // Redirect to the user's profile page
      return res.redirect(`/users/profile/${req.user.username}`);
    }
    // Render the login form
    res.render('users/login', {loggedInUser: req.user}); 
};
  
const changePasswordForm = (req, res) => {
  res.render('users/change-password', {
    loggedInUser: req.user,
    error: req.query.error,
    message: req.query.message
  }); 
};
  
const userProfile = async (req, res) => {
    try {
      const user = await User.findOne({ username: req.params.username });
      if (!user) {
        return res.status(404).json({ error: 'User not found' });
      }
      const userPhotos = await Photo.find({ user: user._id });
      // Render the user profile page
      res.render('users/user-profile', {userProfile: user, loggedInUser: req.user, userPhotos: userPhotos}); 
    } catch (error) {
      console.log(error)
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

const logoutUser = (req, res) => {
    // Use req.logout() to log out the current user
    req.logout(function(err) {
        if (err) { return next(err); }
        res.redirect('/');
    });
};

module.exports = {
    registerUser,
    loginUser,
    changePassword,
    changeDescription,
    registerForm,
    loginForm,
    changePasswordForm,
    userProfile,
    logoutUser,
};