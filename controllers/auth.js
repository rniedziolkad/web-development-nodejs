// middleware/authenticate.js
const ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next(); // User is authenticated, proceed to the next middleware or route
    }
  
    // User is not authenticated, redirect or send an unauthorized response
    res.redirect('/users/login');
};
  
module.exports = { ensureAuthenticated };
  